#############################################################################
#This script converts audio (music) files between formats using ffmpeg and  #
#mapping only the audio track of each file, then the embedded pictures will #
#be removed. This script lists recursively the directory in which runs      #
#(running the script from a directory in which isn't located MAY CAUSE      #
#ERRORS) and converts ONLY the files it finds in subdirectories. Once each  #
#conversion finishes the script checks the exit status of the conversion,   #
#if succesful the input file is removed.                                    #
#This is a test project and is licensed with the GPLv2 in order to avoid    #
#license issues with the FFMPEG project.                                    #
#																			#
#Copyright (C) 2020 Diametralls6C											#
#																			#
#This program is free software; you can redistribute it and/or				#
#modify it under the terms of the GNU General Public License				#
#as published by the Free Software Foundation; either version 2				#
#of the License, or (at your option) any later version.						#
#																			#
#This program is distributed in the hope that it will be useful,			#
#but WITHOUT ANY WARRANTY; without even the implied warranty of				#
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the				#
#GNU General Public License for more details.								#
#																			#
#You should have received a copy of the GNU General Public License			#
#along with this program; if not, write to the Free Software				#
#Foundation, Inc., 51 Franklin Street, Fifth Floor, 						#
#Boston, MA  02110-1301, USA.												#
#############################################################################
#!/bin/bash

IFS=$'\n' #This avoid errors related with file names.
BDIR=$PWD #Base directory, the script only converts files in subdirectories.
LOG="log"
IF="mp3" #Input format.
OF="ogg" #Output format.
#Markers
FC=0
ST=0
CT=0
MC=0
OC=0

let "ST=$(date +%s)"
for dir in $(ls -R | sed -n '/^\.\/.*:$/'p | sed -E 's/.\/(.*):/\1/'); do
	cd "${dir}"/
	for file in *."$IF"; do
		echo "${dir}"/"${file}" >> "${BDIR}"/"${LOG}"
		if [ -e "${file}" ]; then
			let "CT = $(date +%s) - $ST"
			echo -e "$FC files converted in $CT seconds.\nProcessed kB: $MC -> $OC.\n" >> "${BDIR}"/"${LOG}"
			let "MC = $(ls -s  "${file}" | sed -E 's/[[:space:]]*([[:digit:]]*)[[:space:]].*/\1/') + $MC"
			ffmpeg -i "${file}" -map a "${file/%"$IF"/"$OF"}"		
			if [ $? == 0 ]; then
				rm "${file}"
				sync
			else
				rm "${file/%"$IF"/"$OF"}"
			fi			
			let "OC = $(ls -s  "${file/%"$IF"/"$OF"}" | sed -E 's/[[:space:]]*([[:digit:]]*)[[:space:]].*/\1/') + $OC"
			let "FC = $FC + 1"
		else
			echo "No ""$IF"" files in directory." >> "${BDIR}"/"${LOG}"
		fi
	done
	cd "${BDIR}"
done

sync
let "CT = $(date +%s) - $ST"
echo -e "\nCompleted: $FC files converted in $CT seconds.\nSize change in kB: $MC -> $OC.\n" >> "${BDIR}"/"${LOG}"
echo -e "\nCompleted: $FC files converted in $CT seconds.\nSize change in kB: $MC -> $OC.\n"
